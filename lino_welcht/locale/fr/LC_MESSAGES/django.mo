��    H      \  a   �            !     2     :     F     O     V     b     i     z     �     �     �     �     �  
   �     �     �     �     �     �     �       	             (     8     I     R     d     l     x          �     �     �  +   �     �     �     �            
        *     -     6     D  	   S     ]     l     s     �     �     �     �     �     �     �     �  
   �     	     	     %	     1	     9	     E	     X	     l	     u	     �	  	   �	     �	  �  �	     #  
   3     >  
   J     U     ^     r     �     �     �     �     �     �     �     �     �     �                    &     ?     K     W     d     y     �     �  	   �     �     �  
   �     �     �     �  6     )   8     b     j     y     �     �     �     �     �     �     �     �     �     �               !     7     N     j     �     �     �     �     �       	     
   &     1     @     P     X     k     }     �     ,   <      #      '   @   (                   B          :              5   ?   &                 G   	   3       9   E          *   /      -   F   )   2                                      %   1   0                               >   7           !   
   C   H         .      D   4   8           6       "   =       ;          $   A                +        Active workshops Advisor Attestation Calendar Career Certificate Client Closed workshops Coach Coaches Competences Considered active Contact Contact details Creativity Debts Declarative Designation Detected by Diploma Draft workshops Duties Enrolment Evaluations Family problems Family situation Finished Followed by FORem General Get active! Health History IO Inactive workshops Integration workshops Introduction to basic kitchen technologies. Job search workshops Kitchen Learning Mathematics Miscellaneous Never came No Obstacle Obstacle type Obstacle types Obstacles Our first baby Person Psycho-social intervention Reference name Remark Request for enrolment Request for intervention Skill Skill proof Skill proofs Skills Soft skill Soft skill type Soft skill types Soft skills Started Translation Unemployment right Unemployment rights Workshop Workshop lines Workshop series Workshops Yes Project-Id-Version: lino.apps.dsbe
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-08 13:15+0300
Last-Translator: Luc Saffre <luc.saffre@gmail.com>
Language-Team: fr <lino-users@gmail.com>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n>1;
Generated-By: Babel 2.12.1
X-Generator: Poedit 3.0.1
 Ateliers actifs Conseiller Attestation Calendrier Parcours Certificat médical Bénéficiaire Ateliers terminés Intervenant Intervenants Compétences Considéré actif Contact Coordonnées Créativité Dettes Déclarative Désignation Détecté par Diplôme Ateliers en préparation Obligations Inscription Évaluations Problèmes familiers Situation familiale Terminé Suivi FORem Général Activons-nous! Santé Historique O.I. Ateliers inactifs Ateliers d'insertion sociale Introduction aux techniques de cuisine élémentaires. Ateliers d'Insertion socioprofessionnelle Cuisine Avertissements Mathématiques Divers Jamais venu Non Freins Type de frein Types de freins Freins Notre premier bébé Personne Intervention psycho-sociale Nom de référence Remarque Demande d'inscription Demande d'intervention Compétence professionnelle Preuve de qualification Preuves de qualification Compétences professionnelles Compétence sociale Type de compétence sociale Types de compétence sociale Compétences sociales Commencé Traduction Droit chômage Droits chômage Atelier Séries d'ateliers Série d'ateliers Ateliers Oui 